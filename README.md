# Convert genotypes to mapping files

Le programme offre 4 systèmes de conversion. Les formats obtenus permettent donc d'utiliser les logiciels suivants:  

 * Pour la cartographie avec Mapmaker et la visualisation avec GGT (format [A, B, H, -] obtenu avec l'option -c 0)  
 * Pour la cartographie avec QTL IciMapping (format [2, 0, 1, -1] obtenu avec l'option -c 1)  
 * Pour la sélection génomique avec rrBLUP (format [1, -1, 0, NA] obtenu avec l'option -c 2)  
 * Pour la cartographie avec MSTmap et GenotypeCorrector (format [A, B, X, -] obtenu avec l'option -c 3)  

Par exemple, en choisissant l'option -c 3 vous aurez le système [A,B,X,-] et la correspondance entre les données
originales et le nouveau format sera la suivante:  

 - un génotype semblable à celui du parent 1: A  
 - un génotype semblable à celui du parent 2: B  
 - un génotype hétérozygote: X  
 - une donnée manquante: -    
  
  
**Ce qui aussi possible de faire avec le programme**  

 * de conserver les génotypes hétérozygotes ou de les transformer en données manquantes  
 * de conserver ou non les génotypes des parents  
 * de transposer les fichiers (converti et corrigé) de sortie  
 * de remplacer les possibles doubles recombinants (double crossing over ou dco) en données manquantes  
 

**Filtration des loci en fonction des résultats des parents**

Avant d'effectuer toute conversion, le programme analyse les résultats obtenus pour les 2 parents et ne conserve que les loci qui rencontrent les conditions suivantes:  
  
 * les deux parents doivent être homozygotes avec des génotypes différents  
 * aucun des deux parents ne doit être hétérozygote  
 * aucun des deux parents ne doit avoir une donnée manquante  
 
 
**Exemple d'une ligne de commande minimale. Tous ces arguments doivent figurer dans la ligne de commande**  
```
./convert2map.py -i TS2018_8EX10S.hmp -p1 Parent1 -p2 Parent2 -hmp
```
 - Le nom du fichier input (argument -i)  
 - Le nom du parent 1 (argument -p1)  
 - Le nom du parent 2 (argument -p2)  
 - Le format du fichier input (argument -vcf ou -hmp)  
  
**Liste des paramètres par défaut, entre parenthèses l'option pour modifier la valeur du paramètre**  

 - le code de conversion est 0 pour A,B,H,- (-c [0,1,2,3])  
 - les génotypes hétérozygotes sont conservés et transformés selon le code choisi (-nohet)  
 - les génotypes des parents ne sont pas conservés (-p)  
 - les possibles dco ne sont pas transformés en données manquantes (-dco)  
 - il n'y a pas de création de fichier transposé (-t)  
 - le fichier de sortie est output_conv.txt (-o <output file name>)  

**En vrac, veuillez noter les points suivants**  

 - si la transformation des dco en données manquantes est demandée, le programme ne transforme pas les données localisées dans la première et la dernière ligne d'un bloc chromosomique.  
 - les données des parents peuvent être à n'importe quel endroit dans le fichier vcf ou hapmap  
 - lorsque la transposition est demandée, tout les fichiers de sortie (converti et corrigé si dco est activé) auront une version transposée  


**Détails obtenus avec le help**  

```
./convert2map.py -h
usage: convert2map.py [-h] [-i INPUT_FILE] [-p1 P1] [-p2 P2] [-vcf | -hmp]
                      [-c {0,1,2,3}] [-nohet] [-p] [-o OUTPUT_FILE] [-t]
                      [-dco]

optional arguments:
  -h, --help            show this help message and exit
  
  -i INPUT_FILE, --input_file INPUT_FILE
                        Name of the input file

  -p1 P1, --parent1 P1       Name of Parent 1
 
  -p2 P2, --parent2 P2       Name of Parent 2
 
  -vcf, --file_format_vcf
                        Turn on if the input file format is vcf
 
  -hmp, --file_format_hapmap
                        Turn on if the input file format is hapmap
  
  -c {0,1,2,3}, --code_genotype {0,1,2,3}
                        0 (A,B,H,-), 1 (2,0,1,-1), 2 (1,-1,0,NA), 3 (A,B,X,-)
                        Explanation: (parent1, parent2, heterozygote,
                        missing). Codification by default: 0 for (A,B,H,-). If
                        ever a call is not recognized by convert2map an error
                        'E' will be appended.
  
  -nohet, --hetero_remove
                        Activate to transform heterozygotes into missing data
  
  -p, --parent_conserve
                        Activate to keep parents in the output file
                        
  -o OUTPUT_FILE, --output_file OUTPUT_FILE
                        Name the output file ('output_conv.txt' by default)
 
  -t, --transpose       Activate if you want the output file to be
                        transposed
                        and sample as column
  
  -dco, --change_dco    The double crossing over (dco) genotypes are changed to
                        missing data
```


Exemple de lignes de commande:  
```
./convert2map.py -i TS2018_8EX10S.hmp -p1 Parent1 -p2 Parent2 -hmp  

./convert2map.py -i TS2018_8EX10S.hmp -p1 Parent1 -p2 Parent2 -hmp -t  

./convert2map.py -i TS2018_8EX10S.hmp -p1 Parent1 -p2 Parent2 -hmp -t -dco  

./convert2map.py -i TS2018_8EX10S.hmp -p1 Parent1 -p2 Parent2 -hmp -t -dco -c 2  

./convert2map.py -i TS2018_8EX10S.hmp -p1 Parent1 -p2 Parent2 -hmp -t -dco -c 2 -o TS2018_8EX10S.conv  

./convert2map.py -i TS2018_8EX10S.hmp -p1 Parent1 -p2 Parent2 -hmp -o TS2018_8EX10S.conv -code 2 -p -dco  

./convert2map.py -i TS2018_8EX10S.hmp -p1 Parent1 -p2 Parent2 -hmp -o TS2018_8EX10S.conv -code 3 -p -dco -t  

./convert2map.py -i TS2018_8EX10S.vcf -p1 Parent1 -p2 Parent2 -vcf -o TS2018_8EX10S.conv -code 3 -nohet -p -dco -t  

./convert2map.py -i TS2018_8EX10S.vcf -p1 Parent1 -p2 Parent2 -vcf -o TS2018_8EX10S.conv -code 1 -dco  
```



