#!/prg/python/3.5/bin/python3.5

import vcf, argparse, csv, sys

#Fonction de conversion des genotypes
def f_converted_call_to_append(call_to_convert, call_p1, call_p2):
#	if (call_to_convert is None) or (call_to_convert in listHez and usr_input.hetero_remove):
	if (call_to_convert in missing) or (call_to_convert in listHez and usr_input.hetero_remove):

	# treatment of missing data and heterozygotes if they have to be converted into missing data
#		return missing[usr_input.missing]  # -1, -, NA, N and 0 are the possible results for missing data
#		print("Missing data!")	
		return codemap[usr_input.code_genotype][3]

	elif call_to_convert == call_p1:  # comparison with the first parent
		return codemap[usr_input.code_genotype][0]  # A, 2 and 1 are the possible results
	elif call_to_convert == call_p2:  # comparison with the second parent
		return codemap[usr_input.code_genotype][1]  # B, 0 and -1 are the possible results
	elif call_to_convert in listHez:  # heterozygote
		return codemap[usr_input.code_genotype][2]  # H, 1 and 0 are the possible results
	else:
		return 'E'
		# if "E" is appended something went wrong.
		# Either convert2map has encountered a symbole not featuring in the lists or that
		# symbol has been put there by mistake

def f_to_be_filtered_out(call_p1, call_p2):
	# Filtration des loci sur la base des genotypes des parents
	# On retire un locus, si celui-ci réponds a au moins une des conditions suivantes:
	# les 2 parents sont homo avec le meme genotype) ou (un des 2 parents est hetero) ou (le genotype d'un des 2 parents est manquant)
	#  (                   2 homo with the same GT                      ) ou (             at least 1 hetero          ) ou (      at least 1 missing data     )
	if (call_p1 in listHoz and call_p2 in listHoz and call_p1 == call_p2):
		#print("Condition F1: les 2 parents sont homozygotes pour le même genotype")
		return True
	
	elif (call_p1 in listHez or call_p2 in listHez):
		#print("Condition F2: Au moins un des deux parents est heterozygote")
		return True
	 
#	elif (call_p1 is None or call_p2 is None):
	elif (call_p1 in missing or call_p2 in missing):
		#print("Condition F3: Au moins un des deux parents a une donnée manquante")
		return True
		
	else:
		return False

#field_id
def f_formated_header(chrom_call, pos_call, no_current_sample, no_current_locus, current_call):
	if field_id:
		if no_current_sample ==0:#In order to know if "Id" as header need to be appended or the actual id of the current locus.
			if no_current_locus ==0:
				return 'Id'
			else:
				return str(chrom_call + spacer_field_id + pos_call)
		elif no_current_sample == 1:#If the Id is displayed, we need to avoid the position.
			pass
		else:#Reaching this point mean that we're treating a sample name.
			return  current_call
	else:#dealing with chrom and pos as separate field
		return current_call

def f_is_locus_header(no_current_sample, no_current_locus):
	if no_current_sample == 0:#Managing the header of each locus.
		return True
	elif no_current_sample == 1 and not field_id: #Managing the header of each locus with chrom and pos as separate field.
		return True
	elif no_current_locus == 0: #The goal is to avoid the header of the file where is the name of the samples.
		return True
	else:
		return False

if __name__ == '__main__':

	#list of conventions to make calls (the first item of the list will be used by default)
	#[code for parent 1, code for parent 2, code for heterozygotes, code for missing data]
	codemap=[['A','B','H','-'],['2','0','1','-1'],['1','-1','0','NA'],['A','B','X','-']]

	#help_codemap="Codification type by default:" + str(codemap[0]) + "\n"
	help_codemap=""
	for theIndex, theCode in enumerate(codemap):
		help_codemap += str(theIndex) + " (" + theCode[0] + "," +  theCode[1] + "," +  theCode[2] + "," +  theCode[3] + ")"
		if theIndex < len(codemap)-1:
			help_codemap += ", "
	help_codemap+="\n Explanation: (parent1, parent2, heterozygote, missing)."
	help_codemap+="\n Codification by default: 0 for (A,B,H,-). "
	help_codemap+="\n If ever a call is not recognized by convert2map "
	help_codemap+="\n an error 'E' will be appended."
		
	parser = argparse.ArgumentParser()
	groupFileFormat = parser.add_mutually_exclusive_group()

	parser.add_argument('-i','--input_file', help='Name of the input file')
	parser.add_argument('-p1','--parent1', type=str, help="Name of Parent 1")
	parser.add_argument('-p2','--parent2', type=str, help="Name of Parent 2")
	
	#Options related to the file format
	
	#file_format_vcf=False
	#file_format_hapmap=False
	groupFileFormat.add_argument('-vcf', '--file_format_vcf', action="store_true", help="Turn on if the input file format is vcf")
	groupFileFormat.add_argument('-hmp', '--file_format_hapmap', action='store_true', help="Turn on if the input file format is hapmap")

	#Miscellaneous options
	parser.add_argument('-c', '--code_genotype', type=int, choices=range(0, len(codemap)), help=help_codemap, default=0)					
	parser.add_argument('-nohet', "--hetero_remove", action="store_true",	help="Activate to transform heterozygotes into missing data")

	#Options related to the parents
	parser.add_argument('-p', "--parents_conserve", action="store_true", help="Activate to keep parents in the output file")

	#Options related to the output file
#	output_file_name='output.txt'
	parser.add_argument('-o', '--output_file', type=str, help="Name the output file ('output_conv.txt' by default)", default='output_conv.txt')
	parser.add_argument('-t', '--transpose', action='store_true', help='Activate if you want the output file to be transposed')

	#Options related to the correction
	parser.add_argument('-dco', '--change_dco', action='store_true', help='The double crossing over (dco) genotypes are changed to missing data')
	
#Gestion des erreurs dans la ligne de commande
	
	if len(sys.argv) < 2:
		parser.print_help()
		sys.exit(1)

	usr_input = parser.parse_args()

#	print(usr_input)

	if usr_input.input_file==None:
		print("Veuillez specifier le fichier a analyser svp!")
		sys.exit(1)

	if usr_input.file_format_vcf==False and usr_input.file_format_hapmap==False:
		print("Veuillez specifier le format du fichier en input")
		sys.exit(1)
	
	if usr_input.parent1==None:
		print("Veuillez specifier le nom du parent 1 svp!")
		sys.exit(1)
		
	if usr_input.parent2==None:
		print("Veuillez specifier le nom du parent 2 svp!")
		sys.exit(1)


	#-----------------------------------------
	#format the header
	header=[]
	field_id=True
	spacer_field_id='_'
	header.append("Id")
	#-----------------------------------------

	#exclusion list of homozygous genotypes
	listHoz = ['A/A','T/T','C/C','G/G','A','T','C','G']

	#exclusion list of heterozygous genotypes
	listHez = ['A/T','A/C','A/G','T/A','C/A','G/A', 'T/C', 'T/G', 'C/T', 'G/T','C/G', 'G/C','R', 'Y', 'K', 'M', 'S', 'W']

	#Matrice des genotypes possibles pour la conversion 
	genot={None:None,'A/A':'A','T/T':'T','C/C':'C','G/G':'G','N/N':'N','A/T':'W','A/C':'M','A/G':'R','T/A':'W','C/A':'M','G/A':'R', 'T/C':'Y', 'T/G':'K', 'C/T':'Y', 'G/T':'K','C/G':'S', 'G/C':'S'}

	#Liste des possibilités de données manquantes
	missing=['N','-','N/N','.','./.',None]

	#a list of all the loci that need to be put in the output file
	output_file_content = []

	#for stats
	#the number of loci removed by filtration (this variable is used for statistic only)
	nb_loci_removed_by_filtration_stat = 0

	#the number of loci submitted to filtration (this variable is used for statistic only)
	nb_loci_submitted_stat = 0

	#-----------------------------------------------------------------------------------------------------
	#DEBUT DE LA SECTION TRAITANT LES FICHIERS EN FORMAT VCF
	if usr_input.file_format_vcf:
		print("\nLecture d'un fchier en format VCF\n")
	
		# create the output file
		if usr_input.output_file=="output_conv.txt":
			regular_output_file = open(usr_input.output_file, 'w')

		else:
			dot = usr_input.output_file.index(".")
			ext = usr_input.output_file[dot+1:]
			output = usr_input.output_file[:dot]
			regular_output_file = open(output+'_conv.txt', 'w')

		# ------------------------------------
		#formatting the header
		try:
			loci=vcf.Reader(open(usr_input.input_file,"r"))
		except:
			print("Le fichier",usr_input.input_file,"n'existe pas!")
			sys.exit(1)

		for sample in loci.samples:
			if sample == usr_input.parent1 or sample == usr_input.parent2:
				if usr_input.parents_conserve:
					#print("parents_conserve")
					header.append(sample)
			else:
				#print("No_parents",sample)
				header.append(sample)
		#print(header)
		output_file_content.append(header)
		#built and write the header of the map file with the name of the samples
		# --------------------------------------

		for locus in loci:
			
			nb_loci_submitted_stat+=1

			#Takes the genotype of parent 1 for the current locus
			try:
				call_p1=locus.genotype(usr_input.parent1)
			except:
				print("Le nom donné au parent 1,",usr_input.parent1,", n'existe pas dans le fichier\n")
				sys.exit(1)
			#print(call_p1)
			#Takes the genotype of parent 2 for the current locus
			try:
				call_p2=locus.genotype(usr_input.parent2)
			except:
				print("Le nom donné au parent 2,",usr_input.parent2,", n'existe pas dans le fichier\n")
				sys.exit(1)
				
			#print(call_p2)
			#print(locus.CHROM, locus.POS, "P1: ", call_p1.gt_bases, "P2: ", call_p2.gt_bases)
			#Filtration beginning-------------------------------
			#Filtration des loci sur la base des genotypes des parents
			#On retire un locus, si celui-ci réponds a au moins une des conditions suivantes:
			#les 2 parents sont homo avec le meme genotype) ou (un des 2 parents est hetero) ou (le genotype d'un des 2 parents est manquant)
			if f_to_be_filtered_out(call_p1.gt_bases, call_p2.gt_bases):
				nb_loci_removed_by_filtration_stat += 1
				#Filtration end---------------------------

			#Conversion beginning-------------------------
			#Conversion des genotypes dans le nouveau code
			#Seuls les loci qui passent l'etape de filtration entrent dans cette boucle
			else:
#				print("no loci filtration")
			#only the case 2 Homo of GT different remains to convert to ABH; 2,0,1 or 1, -1,0
				list_conv = []

				list_conv.append(locus.CHROM + '_' + str(locus.POS))

				s=0
				for sample in loci.samples:
					s+=1
					#print(locus.genotype(sample))
					call_to_convert=locus.genotype(sample)
					#print("call_to_convert: ",call_to_convert)
					#print("call_to_convert.gt_bases: ",call_to_convert.gt_bases)
					#if call_to_convert.gt_bases in listHez:
					#	print("Hetero!")
					#else:
					#	pass

					#if call_to_convert.gt_bases is not None:					
					call_to_convert_gt = genot[call_to_convert.gt_bases]
						
					call_p1_gt = genot[call_p1.gt_bases]
					call_p2_gt = genot[call_p2.gt_bases]

					if sample == usr_input.parent1 or sample == usr_input.parent2:
						#print("Parents")
						#treatment of parents
						if usr_input.parents_conserve:
							if sample == usr_input.parent1:
								list_conv.append(codemap[usr_input.code_genotype][0])
							elif sample == usr_input.parent2:
								list_conv.append(codemap[usr_input.code_genotype][1])
							#print("parents_conserve_YES",list_conv)
						else:
							pass
							#list_conv.append(codemap[usr_input.code_genotype][1])
							#print("parents_conserve_NO",list_conv)
					else:
						#treatment of samples
						#print("Tout les autres cas",list_conv)
						#print(call_to_convert_gt, call_p1_gt, call_p2_gt)
						#print(f_converted_call_to_append(call_to_convert_gt, call_p1_gt, call_p2_gt))
						list_conv.append(f_converted_call_to_append(call_to_convert_gt, call_p1_gt, call_p2_gt))
				#print(list_conv)
				output_file_content.append(list_conv)
		#to produce a regular output file (not transposed)
		for sample in output_file_content:
			regular_output_file.write('\t'.join(sample) + '\n')

	#FIN DE LA SECTION TRAITANT LES FICHIERS EN FORMAT VCF
	#-----------------------------------------------------------------------------------------------------

	#-----------------------------------------------------------------------------------------------------
	#DEBUT DE LA SECTION TRAITANT LES FICHIERS EN FORMAT HAPMAP

	elif usr_input.file_format_hapmap:
		print("\nLecture d'un fichier en format HAPMAP\n")
		
		try:
			test=open(usr_input.input_file,"r")

		except:
			print("Le fichier",usr_input.input_file,"n'existe pas!")
			sys.exit(1)
		
		hapmap_column_to_avoid = [0, 1, 4, 5, 6, 7, 8, 9, 10]
		# Avoiding the unwanted fields namely: rs, alleles, strand, assembly, centre, prostLSID, assayLSID, panelLSID and QCcode

		if usr_input.output_file=="output_conv.txt":
			regular_output_file = open(usr_input.output_file, 'w')

		else:
			dot = usr_input.output_file.index(".")
			ext = usr_input.output_file[dot+1:]
			output = usr_input.output_file[:dot]
			regular_output_file = open(output+'_conv.txt', 'w')

		no_sample_p1=0
		no_sample_p2=0
		hapmap_file_list=[]

		# for stats
		nb_loci_removed_by_filtration_stat = 0
		# the number of loci removed by filtration (this variable is used for statistic only)
		nb_loci_submitted_stat = 0
		# the number of loci submitted to filtration (this variable is used for statistic only)

		locus_nb=0
		# This loop as the duty to fill "hapmap_file_list", which is a list of lists containing the chromosome,
		# the position and the genotypes of each loci,to find the parents and to put their loci(column) number into the
		# no_sample_p1 and no_sample_p2 variables.
		with open(usr_input.input_file) as file_hapmap:
			hapmap = csv.reader(file_hapmap, delimiter='\t')
			for locus in hapmap:
				loci_list = []
				no_current_sample = 0
				for call in locus:
					if no_current_sample not in hapmap_column_to_avoid:
						loci_list.append(call)
						if call in (usr_input.parent1, usr_input.parent2) and locus_nb == 0 or no_current_sample in (no_sample_p1, no_sample_p2):
						# treating the parents
							if call == usr_input.parent1 or no_current_sample == no_sample_p1:
								no_sample_p1 = no_current_sample
							else:
								no_sample_p2 = no_current_sample
					no_current_sample+=1
				hapmap_file_list.append(loci_list)
		
		#print(no_sample_p1,no_sample_p2)
		#print(hapmap_file_list)
		
		is_header = True
		for no_current_locus in range(0, len(hapmap_file_list)):
			#print(no_current_locus)
			list_conv = []

			nb_loci_submitted_stat += 1

			# Takes the genotype of parent 1 for the current locus
			call_p1 = hapmap_file_list[no_current_locus][no_sample_p1-9]
			#The '-9' is there because of the 9 columns that are in the original file, but not in the "hapmap_file_list"
			# Takes the genotype of parent 2 for the current locus
			call_p2 = hapmap_file_list[no_current_locus][no_sample_p2-9]
			#The '-9' is there because of the 9 columns that are in the original file, but not in the "hapmap_file_list"

			# Filtration beginning------------------------------------------------------------------------------------------
			#print(call_p1, call_p2)
			if f_to_be_filtered_out(call_p1, call_p2):
				nb_loci_removed_by_filtration_stat += 1
			# Filtration end------------------------------------------------------------------------------------------------

			# Conversion beginning------------------------------------------------------------------------------------------
			else:  # only the case 2 Homo of GT different remains to convert to ABH; 2,0,1 or 1, -1,0
				#print("no loci filtration")
			#	list_conv = []

				for no_current_sample in range(0, len(hapmap_file_list[0])):
					#print(no_current_sample)
					#print(no_current_sample)
					if not (no_current_sample in (no_sample_p1-9, no_sample_p2-9)) or usr_input.parents_conserve:
					#Making sure that the parents do not appear in the output file, unless the user specify that he want them to be in it.
						current_call = hapmap_file_list[no_current_locus][no_current_sample]
						#print(current_call)
						if f_is_locus_header(no_current_sample, no_current_locus):
							chrom_call = hapmap_file_list[no_current_locus][0]
							pos_call = hapmap_file_list[no_current_locus][1]
							content_to_append = f_formated_header(chrom_call, pos_call, no_current_sample, no_current_locus, current_call)
							if content_to_append != None:
								list_conv.append(content_to_append)
						elif  no_current_sample == 1:
						#If field_id is activated no_current_sample = 1 need to be skipped otherwise the id and the position will be displayed.
							pass
						else:
							list_conv.append(f_converted_call_to_append(current_call, call_p1, call_p2))
			#print(list_conv)
			if len(list_conv)>0:
				output_file_content.append(list_conv)
			else:
				pass
			
		for sample in output_file_content:
			regular_output_file.write('\t'.join(sample) + '\n')
			
	#FIN DE LA SECTION TRAITANT LES FICHIERS EN FORMAT HAPMAP
	#-----------------------------------------------------------------------------------------------------

	#-----------------------------------------------------------------------------------------------------
	#DEBUT DE LA SECTION PERMETTANT DE TRANSPOSER LE FICHIER DE SORTIE
	
		#This loop is used to write the output map file no matter what the input file format is.
		#It may display the result in the terminal depending on the user's input(verbose).
	if usr_input.transpose:
		print("Transposition du fichier converti\n")
		
		if usr_input.output_file=="output_conv.txt":
			transpose_output_file = open("output_conv_trans.txt", 'w')

		else:
			dot = usr_input.output_file.index(".")
			ext = usr_input.output_file[dot+1:]
			output = usr_input.output_file[:dot]
			transpose_output_file = open(output+'_conv_trans.txt', 'w')
				
		transposed_map=[]
		for no_current_sample in range(0, len(output_file_content[0])):

			current_row = []
			for no_current_locus in range(0, len(output_file_content)):
				current_row.append((output_file_content[no_current_locus][no_current_sample]))
			transposed_map.append(current_row)
			transpose_output_file.write('\t'.join(current_row) + '\n')

	else:
		pass


	#FIN DE LA SECTION PERMETTANT D'ECRIRE LE FICHIER DE SORTIE
	#-----------------------------------------------------------------------------------------------------

	#-----------------------------------------------------------------------------------------------------
	#DEBUT DE LA SECTION DES CORRECTIONS
	
	if usr_input.change_dco:
	
		print("Conversion des DCO en données manquantes\n")
		
		if usr_input.output_file=="output_conv.txt":
			corrected_file = open("output_corr.txt", 'w')

		else:
			dot = usr_input.output_file.index(".")
			ext = usr_input.output_file[dot+1:]
			output = usr_input.output_file[:dot]
			corrected_file = open(output+'_corr.txt', 'w')

		# Besoin de cette liste pour la transposition du fichier de sortie
		corrected_file_list = []
	
		#print(output_file_content)
	
		a=0
		while a < len(output_file_content):
			new_data = []
			
			if a < 2:
			# On ne traite pas l'entete, la premiere ligne de donnees et la premiere ligne de chaque CHROM
				#print(a,'header or first data line',output_file_content[a])
				#print("Entete ou premiere ligne de donnees",output_file_content[a])
				corrected_file.write('\t'.join(output_file_content[a]) + '\n')
				corrected_file_list.append(output_file_content[a])
				a+=1
			
			elif  a == len(output_file_content)-1:
				#print("Derniere ligne",output_file_content[a])
				corrected_file.write('\t'.join(output_file_content[a]) + '\n')
				corrected_file_list.append(output_file_content[a])
				a+=1	
			
			else:
				#print(output_file_content[a][0].split('_')[0],"<-->",output_file_content[a+1][0].split('_')[0])
				if output_file_content[a][0].split('_')[0] != output_file_content[a+1][0].split('_')[0]:
					#print("Derniere ligne d'un chromosome",output_file_content[a][0].split('_')[0],"<-->",output_file_content[a+1][0].split('_')[0])
					#print("Derniere ligne d'un chromosome",output_file_content[a])
					corrected_file.write('\t'.join(output_file_content[a]))
					corrected_file_list.append(output_file_content[a])
		
				elif output_file_content[a][0].split('_')[0] != output_file_content[a-1][0].split('_')[0]:
					#print("Premiere ligne d'un chromosome",output_file_content[a][0].split('_')[0],"<-->",output_file_content[a-1][0].split('_')[0])
					#print("Premiere ligne d'un chromosome",output_file_content[a])
					corrected_file.write('\t'.join(output_file_content[a]))
					corrected_file_list.append(output_file_content[a])

				else:
				#Traitement de toutes les autres lignes de donnees
					#print(a,'Regular data line',output_file_content[a])
				
					new_data.append(output_file_content[a][0])
					#Les loci commencent à la position 2 de chaque liste
					b=1
					while b < len(output_file_content[0]):
						if (output_file_content[a][b] != output_file_content[a-1][b]) and (output_file_content[a][b] != output_file_content[a+1][b]):
							#correction d'un dco en donnees manquante
							#new_data.append('NA')
							new_data.append(codemap[usr_input.code_genotype][3])
						
						else:
							#Pas de correction
							new_data.append(output_file_content[a][b])
						b+=1
					corrected_file_list.append(new_data)
				
				corrected_file.write('\t'.join(new_data) + '\n')
				
				a+=1
		
		if usr_input.transpose:
			print("Transposition du fichier corrigé pour les dco\n")

			if usr_input.output_file=="output_conv.txt":
				transpose_corrected_file = open("output_corr_trans.txt", 'w')

			else:
				dot = usr_input.output_file.index(".")
				ext = usr_input.output_file[dot+1:]
				output = usr_input.output_file[:dot]
				transpose_corrected_file = open(output+'_corr_trans.txt', 'w')

			for locus_nb in range(0, len(corrected_file_list[0])):
				test = []
				for sample_nb in range(0, len(corrected_file_list)):
					transpose_corrected_file.write(corrected_file_list[sample_nb][locus_nb] + '\t')
				transpose_corrected_file.write('\n')

	#FIN DE LA SECTION DES CORRECTIONS
	#-----------------------------------------------------------------------------------------------------


# basic stats to be displayed in the terminal
#print("convert2map filtration removes", nb_loci_removed_by_filtration_stat, "loci out of the", nb_loci_submitted_stat, "submitted\n")
